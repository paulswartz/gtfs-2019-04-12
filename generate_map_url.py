# take it to https://console.cloud.google.com/google/maps-apis/apis/static-maps-backend.googleapis.com/staticmap?project=api-project-23632719142&duration=P14D for signing later
import requests
import os
import collections
from urllib.parse import quote
import hashlib
import hmac
import base64
import webbrowser

COLORS = {
    "0": "purple",
    "1": "red",
    "2": "green",
    "I": "green"
}
LABELS = {
    "0": "",
    "1": "S",
    "2": "E",
    "I": "I"
}

SIZES = {
    "0": "tiny"
}
BASE_URL = f"/maps/api/staticmap?size=640x640&scale=2&zoom=17&key={os.environ['GOOGLE_MAPS_API_KEY']}"
STYLES = [
    "feature:poi|visibility:off",
    "feature:transit.station|visibility:off"
]

r = requests.get("https://api-v3.mbta.com/stops/place-sstat?include=child_stops,facilities&fields[facility]=name,type,latitude,longitude").json()
pins = collections.defaultdict(list)

def add_pin(item):
    attributes = item["attributes"]
    if attributes.get("latitude") is None:
        return
    t = str(attributes.get("location_type", attributes.get("type")))[0]
    if t == "2":
        if attributes["wheelchair_boarding"] != 1:
            t = "I"
    pins[t].append(f"{attributes['latitude']},{attributes['longitude']}")

add_pin(r["data"])
for item in r["included"]:
    add_pin(item)

url_parts = [BASE_URL, f"center={r['data']['attributes']['latitude']},{r['data']['attributes']['longitude']}"]
for (label, locations) in pins.items():
    marker_parts = [f"size:{SIZES.get(label, 'mid')}|color:{COLORS.get(label, 'yellow')}|label:{LABELS.get(label, label)}"] + locations
    url_parts.append("markers=" + quote("|".join(marker_parts)))
for style in STYLES:
    url_parts.append("style=" + quote(style))
url = "&".join(url_parts)

def make_digest(message, key):
    key = base64.urlsafe_b64decode(bytes(key, 'UTF-8'))
    message = bytes(message, 'UTF-8')

    digester = hmac.new(key, message, hashlib.sha1)
    digest = digester.digest()
    signature = base64.urlsafe_b64encode(digest)
    return str(signature, 'UTF-8')

signature = make_digest(url, os.environ["GOOGLE_MAPS_SIGNING_KEY"])
url = f"https://maps.googleapis.com{url}&signature={signature}"
webbrowser.open(url)
