# GTFS

2019-04-12

mapping tools workshop

paul swartz, CTD

Note: Hi there! I'm Paul Swartz, lead architect in CTD, and today I'll be doing a quick overview of GTFS.
---
## What is GTFS?

---
### General

### Transit

### Feed

### Specification

Note: "common format for public transportation schedules and associated geographic information"

---
MBTA_GTFS.zip
 - stops.txt
 - facilities.txt
 - facilities_properties.txt
 - facilities_properties_definitions.txt
 - routes.txt
 - shapes.txt
 - pathways.txt
 - ...

Note: Basically, it's a bunch of spreadsheets in text format. Today, I'll just be talking about a few of them that are particularly relevant to mapping.

---
# stops.txt

stops

entrances

platforms

Note: example, South Station. place-sstat, stops for the entrances, stops for red line platforms, CR platforms, silver line platforms

---
# stops.txt

- ID
- name
- lat/lon
- description (some)
- street address (some)
- wheelchair accessibility
- zone (soon)

Note: updates at least once per quarter, but can be more frequent (shuttles).

---
# facilities.txt

- elevator / escalator / ramp / mini-high
- cars: parking (lots and garages), pickup/dropoff, taxi, EV charger
- bike storage
- FVM and retail sales locations

---
# facilities.txt

- ID
- type
- associated stop ID
- name
- lat/lon (some)
- more detailed information in facilities_properties.txt

---
<!-- .slide: data-background="assets/staticmap.png" data-background-size="contain" -->

---
# shapes.txt

detailed path of where a route goes

Note: list of lat/lon, only include routes which are running in the next months

---
# pathways.txt

detailed modeling within/between stations

Note: Recently landed, not fully implemented in our GTFS yet

---
## lots more!

routes

trips

schedules

transfers

holidays

---
# Documentation

https://www.mbta.com/developers/gtfs

---
## Questions?

pswartz@mbta.com
